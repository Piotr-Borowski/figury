package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;
	
	private List<Thread> threadList = new ArrayList<Thread>();
	private Thread thread;
	
	private int delay = 30;

	private Timer timer;

	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	void addFig() {
		Figura fig = null;
		
		if(numer%3==0) {
			fig = new Kwadrat(buffer, delay, getWidth(), getHeight());
						numer++;
					} else if (numer%3==1){
						fig = new Elipsa(buffer, delay, getWidth(), getHeight());
						numer++;
					} else if (numer%3==2){
						fig = new Trojkat(buffer, delay, getWidth(), getHeight());
						numer++;
					}
		
		timer.addActionListener(fig);
		thread = new Thread(fig);
		threadList.add(thread);
		thread.start();
	}
	

	@SuppressWarnings("deprecation")
	void animate() {
		if (timer.isRunning()) {
			for(Thread t: threadList) t.suspend();
			timer.stop();
		} else {
			for(Thread t: threadList) t.resume();
			timer.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null); //getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH)
		buffer.clearRect(0, 0, getWidth(), getHeight());
		buffer.setBackground(Color.WHITE);
	
	}
}
